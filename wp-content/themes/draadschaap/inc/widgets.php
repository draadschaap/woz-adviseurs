<?php 
function ds_widgets_init() {
 
    register_sidebar( array(
        'name'          => 'Footer area 1',
        'id'            => 'footer-area-1',
        'before_widget' => '<div class="ds-footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title is-4">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer area 2',
        'id'            => 'footer-area-2',
        'before_widget' => '<div class="ds-footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title is-4">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => 'Footer area 3',
        'id'            => 'footer-area-3',
        'before_widget' => '<div class="ds-footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title is-4">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer area 4',
        'id'            => 'footer-area-4',
        'before_widget' => '<div class="ds-footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title is-4">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer area 5',
        'id'            => 'footer-area-5',
        'before_widget' => '<div class="ds-footer-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="title is-4">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => 'News sidebar',
        'id'            => 'news-sidebar',
        'before_widget' => '<div class="ds-news-sidebar">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="title is-6">',
        'after_title'   => '</span>',
    ) );

}
add_action( 'widgets_init', 'ds_widgets_init' );