<?php
function draadschaap_scripts() {
    wp_enqueue_style( 'main-css', get_template_directory_uri() . '/assets/css/main.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_script( 'navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'draadschaap_scripts' );

function block_editor_styles() {
    wp_enqueue_style( 'editor-styles', get_theme_file_uri( 'assets/css/style-editor.css' ), false, '1.0', 'all' );
}
add_action( 'enqueue_block_editor_assets', 'block_editor_styles' );