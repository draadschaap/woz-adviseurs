<?php get_header();?>
<section class="section ds-news-single">
    <div class="container">
        <?php while (have_posts() ) : the_post();?>
        <div class="columns is-multiline">
            <div class="column is-12">
                <a class="ds-news-archive-link" href="<?php echo get_permalink( get_option( 'page_for_posts' ) );?>">&#60; Terug naar nieuwsoverzicht</a>
            </div>
            <div class="column is-12">
                <h1 class="title is-2"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="columns is-variable is-8">
            <div class="column is-9">
            <?php if( get_the_post_thumbnail() ): ?>
                <img class="ds-news-thumbnail-img" src="<?php the_post_thumbnail_url() ?>" alt="post-thumbnail-img">
                <?php if( get_the_post_thumbnail_caption() ): ?>
                    <p class="ds-news-thumbnail-caption"><?php the_post_thumbnail_caption() ?></p>
                <?php endif; ?>
            <?php endif; ?>
                <hr>
                <div class="content">
                <?php the_content(); ?>
                </div>
                <a class="ds-news-archive-link" href="<?php echo get_permalink( get_option( 'page_for_posts' ) );?>">&#60; Terug naar nieuwsoverzicht</a>
            </div>
            <?php if( is_active_sidebar('news-sidebar')): ?>
            <div class="column is-3">
                <?php dynamic_sidebar('news-sidebar'); ?>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>