<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'accordion-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'accordion';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// check if the repeater field has rows of data
if( have_rows('accordion') ):
 	// loop through the rows of data
    while ( have_rows('accordion') ) : the_row(); ?>
    <?php
    $accordion_text = get_sub_field('accordion_text');
    $panel_text = get_sub_field('panel_text'); ?>

    <button class="ds-accordion"><?php echo $accordion_text ?><i class="arrow"></i></button>
    <div class="ds-accordion-panel">
        <p><?php echo $panel_text ?></p>
        <hr>
    </div>

    <?php endwhile;

else :

    echo '<p>Geen vragen toegevoegd</p>';

endif;

?>