<?php get_header(); ?>
<?php echo 'kut'; ?>
		<?php if ( have_posts() ) : ?>
			<?php
			// Start the Loop.
            while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
                ?>
                <h1><?php the_title(); ?></h1>
                <?
				// End the loop.
			endwhile;


			// If no content, include the "No posts found" template.
        else :
			get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
