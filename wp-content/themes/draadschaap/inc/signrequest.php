<?php
/**
 * Register a custom post type called "book".
 *
 * @see get_post_type_labels() for label keys.
 */
function ds_signrequest_init() {
    $labels = array(
        'name'                  => _x( 'SignRequests', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'SignRequest', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'SignRequest', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'SignRequest', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New request', 'textdomain' ),
        'new_item'              => __( 'New request', 'textdomain' ),
        'edit_item'             => __( 'Edit request', 'textdomain' ),
        'view_item'             => __( 'View request', 'textdomain' ),
        'all_items'             => __( 'All requests', 'textdomain' ),
        'search_items'          => __( 'Search requests', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent requests:', 'textdomain' ),
        'not_found'             => __( 'No requests found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No request found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Request Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set request image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove request image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as request image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Request archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into request', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this request', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter request list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Request list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Request list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'signrequest' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 0,
        'menu_icon'          => 'data:image/svg+xml;base64,' . base64_encode('<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="#a0a5aa" d="M218.17 424.14c-2.95-5.92-8.09-6.52-10.17-6.52s-7.22.59-10.02 6.19l-7.67 15.34c-6.37 12.78-25.03 11.37-29.48-2.09L144 386.59l-10.61 31.88c-5.89 17.66-22.38 29.53-41 29.53H80c-8.84 0-16-7.16-16-16s7.16-16 16-16h12.39c4.83 0 9.11-3.08 10.64-7.66l18.19-54.64c3.3-9.81 12.44-16.41 22.78-16.41s19.48 6.59 22.77 16.41l13.88 41.64c19.75-16.19 54.06-9.7 66 14.16 1.89 3.78 5.49 5.95 9.36 6.26v-82.12l128-127.09V160H248c-13.2 0-24-10.8-24-24V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24v-40l-128-.11c-16.12-.31-30.58-9.28-37.83-23.75zM384 121.9c0-6.3-2.5-12.4-7-16.9L279.1 7c-4.5-4.5-10.6-7-17-7H256v128h128v-6.1zm-96 225.06V416h68.99l161.68-162.78-67.88-67.88L288 346.96zm280.54-179.63l-31.87-31.87c-9.94-9.94-26.07-9.94-36.01 0l-27.25 27.25 67.88 67.88 27.25-27.25c9.95-9.94 9.95-26.07 0-36.01z"/></svg>'),
        'supports'           => array( 'title', 'excerpt' ),
    );
 
    register_post_type( 'signrequest', $args );
}
add_action( 'init', 'ds_signrequest_init' );

//SIGNREQUEST FORM
add_action('acf/init', 'my_acf_form_init');
function my_acf_form_init() {

    // Check function exists.
    if( function_exists('acf_register_form') ) {

        // Register form.
        acf_register_form(array(
            'id'       => 'new-signrequest',
            'post_id'  => 'new_signrequest',
            'new_post' => array(
                'post_type'   => 'signrequest',
                'post_status' => 'publish'
            ),
            'field_groups' => array(
                'id'            => '81'
            ),
            'submit_value' => __("Document ondertekenen", 'acf'),
            'html_submit_button'  => '<input type="submit" class="button is-fullwidth is-primary" value="%s" />',
            'return'            => '%post_url%',
        ));
    }
}

//LOAD SIGNREQUEST API
require_once(__DIR__ . '/../../../../vendor/autoload.php');

// Configure API key authorization: Token
function signRequestCreateRequest($email, $name, $address, $city, $userId, $pageUrl){
    $config = SignRequest\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'afd412dedb4d0cbca4acc737311394a0bb95d269');
    $config = SignRequest\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Token');

    $apiInstance = new SignRequest\Api\SignrequestQuickCreateApi(new GuzzleHttp\Client(), $config);

    $data = new \SignRequest\Model\SignRequestQuickCreate();
    $signer = new \SignRequest\Model\Signer();
    $prefillName = new \SignRequest\Model\InlinePrefillTags();
    $prefillAddress = new \SignRequest\Model\InlinePrefillTags();
    $prefillCity = new \SignRequest\Model\InlinePrefillTags();

    $signer->setEmail($email);
    $signer->setLanguage('nl');
    $signer->setEmbedUrlUserId($userId);
    $signer->setRedirectUrl($pageUrl);

    $data->setTemplate('https://signrequest.com/api/v1/templates/0384c1d0-3f52-4acb-9f81-60183520eee1/');
    $data->setFromEmail('schaap@draadschaap.nl');
    $data->setName($name . ' - ' . $address);
    $data->setAutoExpireDays(1);

    $prefillName->setExternalId('client_name');
    $prefillName->setText($name);

    $prefillAddress->setExternalId('client_address');
    $prefillAddress->setText($address);

    $prefillCity->setExternalId('client_city');
    $prefillCity->setText($city);

    $data->setSigners(array($signer));
    $data->setPrefillTags(array($prefillName, $prefillAddress, $prefillCity));

    try {
        $result = $apiInstance->signrequestQuickCreateCreate($data);
        $embedUrl = $result['signers'][1]['embed_url'];
        // echo "<script>window.location = '" . $embedUrl . "'</script>";
        header('Location: ' .$embedUrl);
        exit;
    } catch (Exception $e) {
        echo 'Exception when calling DocumentsApi->documentsCreate: ', $e->getMessage(), PHP_EOL;
    }
}