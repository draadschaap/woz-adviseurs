<?php
function custom_ds_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'draadschaap-blocks',
				'title' => __( 'Draadschaap', 'draadschaap-blocks' ),
			),
		)
	);
}
add_filter( 'block_categories', 'custom_ds_category', 10, 2);

function my_acf_blocks_init() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        // Register a signrequest-form block.
        acf_register_block_type(array(
            'name'              => 'ds-signrequest-form',
            'title'             => __('SignRequest Form'),
            'icon'              => ('forms'),
            'description'       => __('A static SignRequest form block.'),
            'render_template'   => 'template-parts/blocks/signrequest-form/signrequest-form.php',
            'category'          => 'draadschaap-blocks',
        ));
        // Register a accordion block.
        acf_register_block_type(array(
            'name'              => 'ds-accordion',
            'title'             => __('Accordion'),
            'icon'              => ('menu'),
            'description'       => __('A dynamic accordion block.'),
            'render_template'   => 'template-parts/blocks/accordion/accordion.php',
            'enqueue_script'    => get_template_directory_uri() . '/template-parts/blocks/accordion/accordion.js',
            'category'          => 'draadschaap-blocks',
        ));
        // Register a accordion block.
        acf_register_block_type(array(
            'name'              => 'ds-content-box',
            'title'             => __('Content box'),
            'icon'              => ('menu'),
            'description'       => __('A dynamic content box block.'),
            'render_template'   => 'template-parts/blocks/content-box/content-box.php',
            'category'          => 'draadschaap-blocks',
        ));
        // // Register a google maps block.
        // acf_register_block_type(array(
        //     'name'              => 'ds-google-maps',
        //     'title'             => __('Google Maps'),
        //     'icon'              => ('location-alt'),
        //     'description'       => __('A dynamic Google Maps block.'),
        //     'render_template'   => 'template-parts/blocks/google-maps/google-maps.php',
        //     'category'          => 'draadschaap-blocks',
        // ));
    }
}
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'my_acf_blocks_init');
}