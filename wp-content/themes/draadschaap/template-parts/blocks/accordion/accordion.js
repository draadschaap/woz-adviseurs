var acc = document.getElementsByClassName("ds-accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
      panel.style.opacity = null;
    } else {
      panel.style.opacity = 1;
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}