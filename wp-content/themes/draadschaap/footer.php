</div><!-- Closing site content div -->
<footer class="site-footer">
    <div class="section ds-footer-section">
        <div class="container">
            <h2 class="title"><?php echo $_SERVER['HTTP_HOST'] ?></h2>
            <div class="columns">
                <div class="column">
                <?php
                if( is_active_sidebar('footer-area-1')){
                    dynamic_sidebar('footer-area-1');
                };
                ?>
                </div>
                <div class="column">
                <?php
                if( is_active_sidebar('footer-area-2')){
                    dynamic_sidebar('footer-area-2');
                };
                ?>
                </div>
                <div class="column">
                <?php
                if( is_active_sidebar('footer-area-3')){
                    dynamic_sidebar('footer-area-3');
                };
                ?>
                </div>
                <div class="column">
                <?php
                if( is_active_sidebar('footer-area-4')){
                    dynamic_sidebar('footer-area-4');
                };
                ?>
                </div>
                <div class="column">
                <?php
                if( is_active_sidebar('footer-area-5')){
                    dynamic_sidebar('footer-area-5');
                };
                ?>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-bar">
        <div class="container">
            <p>Website gerealiseerd door <a class="is-link" href="https://draadschaap.nl">Draadschaap</a></p>
        </div>
    </div>
</footer>



<?php wp_footer(); ?>

</body>
</html>