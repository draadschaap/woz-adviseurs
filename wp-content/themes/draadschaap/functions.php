<?php
foreach (glob(get_template_directory() . '/inc/*.php') as $filename){
    include $filename;
}

/*
* Let WordPress manage the document title.
* By adding theme support, we declare that this theme does not use a
* hard-coded <title> tag in the document head, and expect WordPress to
* provide it for us.
*/
add_theme_support( 'title-tag' );
add_theme_support( 'editor-styles' );
add_theme_support('post-thumbnails');

/**
 * Add support for core custom logo.
 *
 * @link https://codex.wordpress.org/Theme_Logo
 */
add_theme_support(
    'custom-logo',
    array(
        'height'      => 60,
        'width'       => 200,
        'flex-width'  => false,
        'flex-height' => false,
    )
);

// Add support for full and wide align images.
add_theme_support( 'align-wide' );

// This theme uses wp_nav_menu() in two locations.
register_nav_menus(
    array(
        'menu-1' => __( 'primary', 'draadschaap' ),
    )
);