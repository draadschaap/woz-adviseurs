<?php acf_form_head(); ?>
<?php get_header();?>

<?php get_template_part('template-parts/elements/elements', 'hero'); ?>

<section class="section">
    <div class="container">
        <?php 
        while (have_posts()) {
            the_post();
            the_content();
        }
        ?>
    </div>
</section>

<?php get_footer(); ?>