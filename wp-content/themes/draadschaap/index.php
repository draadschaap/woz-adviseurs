<?php get_header();?>
<?php get_template_part('template-parts/elements/elements', 'hero'); ?>
<section class="section ds-news-archive">
    <div class="container">
        <div class="columns is-multiline">
        <?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                ?>
                <div class="column ds-news-column is-4">
                    <a href="<?php echo the_permalink() ?>">
                        <article class="ds-news-thumbnail">
                            <?php if( the_post_thumbnail() ): ?>
                            <img src="<?php get_the_post_thumbnail_url() ?>" alt="post-thumbnail-img">
                            <?php endif; ?>
                            <h2 class="title is-3"><?php the_title(); ?></h2>
                            <p class="ds-news-tags">
                                <?php
                                // Getting the tags as strings
                                $posttags = get_the_tags();
                                if ($posttags) {
                                    foreach($posttags as $tag) {
                                        if( !next( $posttags ) ) {
                                            echo $tag->name;
                                        } else {
                                            echo $tag->name . '<span class="ds-news-tags-seperator">•</span>'; 
                                        }
                                    }
                                }?>
                            </p>
                        </article>
                    </a>
                </div>
                    <?php
                endwhile;
            else :
                _e( 'Sorry, no posts were found.', 'textdomain' );
            endif; ?>
        </div>
        <?php ds_archive_nav(); ?>
    </div>
</section>
<?php get_footer(); ?>