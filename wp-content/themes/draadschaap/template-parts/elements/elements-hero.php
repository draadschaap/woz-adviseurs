<?php
//Verify page id
if( is_home() ){
    $page_id = get_option('page_for_posts');
} else {
    $page_id = get_the_ID();
}

//get_option was needed to make it work on post archive page
$hero_alignment = get_field('hero_alignment', $page_id);
$hero_alignment_center = '';

$page_title = get_field('page_title', $page_id);
$tagline = get_field('tagline', $page_id);
$hero_text = get_field('hero_text', $page_id);

$hero_image = get_field('hero_image', $page_id);

$call_to_action_type = get_field('call_to_action', $page_id);

$hero_cta_button = get_field('button_attributes', $page_id);
$hero_cta_link = $hero_cta_button['button_link'];
$hero_cta_text = $hero_cta_button['button_text'];
$hero_cta_description = $hero_cta_button['button_description'];

if($hero_alignment == 'align_center'){
    $hero_alignment_center = 'is-center';
}
?>

<section class="section ds-hero-section <?php echo $hero_alignment_center; ?>">
    <div class="container ds-hero-container">
        <div class="columns is-vcentered <?php echo $hero_alignment_center; ?><?php if($hero_alignment == 'align_right'){ echo 'columns-reversed'; }?>">
            <div class="column is-6">

                <?php if( $tagline !== '' ): ?>
                <p class="subtitle is-5"><?php echo $tagline ?></p>
                <?php endif; ?>
                
                <?php if( $page_title !== '' ): ?>
                <h1 class="title is-1"><?php echo $page_title ?></h1>
                <?php endif; ?>
                
                <?php if( $hero_text !== ''): ?>
                <p class="hero-text <?php echo $hero_alignment_center; ?>"><?php echo $hero_text ?></p>
                <?php endif; ?>

                <?php if( $call_to_action_type == 'button'): ?>
                <div class="columns is-vcentered <?php echo $hero_alignment_center; ?>">

                    <?php if( $hero_cta_text !== '' && $hero_cta_link !== ''): ?>
                    <div class="column is-4">
                        <a class="button is-fullwidth is-primary" href="<?php echo $hero_cta_link ?>"><?php echo $hero_cta_text ?></a>
                    </div>
                    <?php endif; ?>

                    <?php if( $hero_cta_description !== ''): ?>
                    <div class="column is-4">
                        <p class="ds-cta-description"><?php echo $hero_cta_description ?></p>
                    </div>
                    <?php endif; ?>

                </div>
                <?php endif; ?>
            </div>
            <?php if( $hero_alignment !== 'align_center' && $hero_image !== false ): ?>
            <div class="column">
                <img src="<?php echo $hero_image ?>" alt="">
            </div>
            <?php endif; ?>
        </div>
        <?php if( have_rows('text_links') && $call_to_action_type == 'text_links' ): ?>
            <div class="columns <?php echo $hero_alignment_center; ?>">
                <div class="column ds-text-links is-center">
                <?php while( have_rows('text_links') ): the_row();

                $hero_text_link_url = get_sub_field('link_url');
                $hero_text_link_text = get_sub_field('link_text');
                
                ?>
                    <a class="is-link" href="<?php echo $hero_text_link_url ?>"><?php echo $hero_text_link_text?></a>
                <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>