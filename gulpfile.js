'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
 
var mainPath = './wp-content/themes/draadschaap/scss/main.scss';
var editorStylePath = './wp-content/themes/draadschaap/scss/style-editor.scss';

sass.compiler = require('node-sass');
 
gulp.task('sass', function () {
  return gulp.src([mainPath, editorStylePath])
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./wp-content/themes/draadschaap/assets/css'));
});
 
gulp.task('watch', function () {
  gulp.watch('./wp-content/themes/draadschaap/scss/**/*.scss', gulp.series('sass'));
});