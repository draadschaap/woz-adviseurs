<?php 
$personal_details = get_field('personal_details');
$object_details = get_field('object_details');
$is_send = get_field('is_send');

//SEND REQUEST
$email = $personal_details['email'];

$initials_last_name = $personal_details['last_name'] . ', ' . $personal_details['initials'];

$address = $object_details['address'];
$postal_code = $object_details['postal_code'];
$place = $object_details['place'];

$full_address = $address . ', ' . $postal_code . ', ' . $place;

//UPDATE POST TITLE
$full_name = $personal_details['first_name'] . " " . $personal_details['last_name'];
$post_update = array(
    'ID'            => get_the_ID(),
    'post_title'    => $full_name . ' - ' . $full_address
);

if ( !$is_send ){
    wp_update_post( $post_update );
    update_field('is_send', true);
    signRequestCreateRequest($email, $initials_last_name, $full_address, $place, get_the_ID(), 'https://www.wozadviseurs.nl');
}
?>

<?php get_header();?>
<?php if( current_user_can('editor') || current_user_can('administrator' && $is_send ) ):  ?>
<section class="section">
    <div class="container">
    <?php 
    if( $personal_details ): ?>
        <ul>
            <?php foreach( $personal_details as $name => $value): ?>
                    <li><b><?php echo $name; ?></b> <?php echo $value; ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
            </br>
    <?php
    if( $object_details ): ?>
        <ul>
            <?php foreach( $object_details as $name => $value): ?>
                    <li><b><?php echo $name; ?></b> <?php echo $value; ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <?php echo $is_send; ?>
    </div>
</section>
<?php endif; ?>

<?php get_footer(); ?>